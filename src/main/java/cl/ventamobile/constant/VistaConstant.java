package cl.ventamobile.constant;

public class VistaConstant {
	public static final String INDEX="index";
	public static final String LOGIN="login";
	
	public static final String CRUD_PRODUCTO="mantenedor_productos";
	public static final String VENTA_ISLA="venta_isla";
	public static final String VENTA_TELEFONO="venta_telefonos";
}
