package cl.ventamobile.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class DatatableResult implements Serializable {
	private static final long serialVersionUID = 8799656478674716638L;
	int draw=1;
	int recordsTotal=0;
	int recordsFiltered=0;
	ArrayList<Object> data=new ArrayList<Object>();
	public int getDraw() {
		return draw;
	}
	public void setDraw(int draw) {
		this.draw = draw;
	}
	public int getRecordsTotal() {
		return recordsTotal;
	}
	public void setRecordsTotal(int recordsTotal) {
		this.recordsTotal = recordsTotal;
	}
	public int getRecordsFiltered() {
		return recordsFiltered;
	}
	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}
	public ArrayList<Object> getData() {
		return data;
	}
	public void setData(ArrayList<Object> data) {
		this.data = data;
	}	
}