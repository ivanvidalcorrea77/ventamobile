package cl.ventamobile.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data public class Proveedor {
	
	@Id
	@Column(name="id_proveedor")
	private int id;
	
	@Column(name="glosa")
	private String descripcion;
}