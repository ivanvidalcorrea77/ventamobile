package cl.ventamobile.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data public class Usuario {
	
	@Id
	@Column(name="login")
	private String usuario;
	
	@Column(name="pass")
	private String contrasena;
}
