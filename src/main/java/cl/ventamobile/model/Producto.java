package cl.ventamobile.model;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data public class Producto {
	
	@Id
	@Column(name="id_producto")
	private int id;
	
	@Column(name="nombre")
	private String nombreProducto;
	
	@Column(name="codigo")
	private String codigoProducto;
	
	@Column(name="glosa")
	private String descripcion;
	
	@Column(name="precio")
	private int precioproducto;
	
	@Column(name="id_proveedor")
	private int idProveedor;
	
	@Column(name="stock")
	private int cantidad;
	
	public String getMontoFormated(long monto){
		DecimalFormatSymbols simb = new DecimalFormatSymbols();
        simb.setDecimalSeparator(',');
        simb.setGroupingSeparator('.');
        DecimalFormat nf = new DecimalFormat("$ ###,###.##",simb);
		return nf.format(monto);
	}
}
