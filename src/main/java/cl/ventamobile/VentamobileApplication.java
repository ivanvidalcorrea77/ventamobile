package cl.ventamobile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VentamobileApplication {

	public static void main(String[] args) {
		SpringApplication.run(VentamobileApplication.class, args);
	}
}
