package cl.ventamobile.controller;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cl.ventamobile.constant.VistaConstant;


@Controller
@RequestMapping("/venta")
public class VentaController {
	private static final Log LOG = LogFactory.getLog(VentaController.class);
	
	@GetMapping("/isla")
	public ModelAndView isla(HttpSession session){
 
		ModelAndView mv = new ModelAndView();
		mv = new ModelAndView(VistaConstant.VENTA_ISLA);
		return mv;
	}
	
	@GetMapping("/telefonos")
	public ModelAndView telefonos(HttpSession session){
 
		ModelAndView mv = new ModelAndView();
		mv = new ModelAndView(VistaConstant.VENTA_TELEFONO);
		return mv;
	}
}
