package cl.ventamobile.controller;


import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cl.ventamobile.constant.VistaConstant;

@Controller
@RequestMapping("/mantenedor")
public class MantenedorProductosController {
	private static final Log LOG = LogFactory.getLog(MantenedorProductosController.class);
	
	@GetMapping("/productos")
	public ModelAndView productos(HttpSession session){

		ModelAndView mv = new ModelAndView();
		
		mv = new ModelAndView(VistaConstant.CRUD_PRODUCTO);
	
		return mv;
	}
}
