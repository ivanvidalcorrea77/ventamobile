package cl.ventamobile.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cl.ventamobile.constant.VistaConstant;

@Controller
@RequestMapping(value={"", "/login"})
public class LoginController {
	private static final Log LOG = LogFactory.getLog(LoginController.class);
	
	@RequestMapping("")
	public ModelAndView index(){
		ModelAndView mv = new ModelAndView(VistaConstant.LOGIN);
		return mv;
	}
}
