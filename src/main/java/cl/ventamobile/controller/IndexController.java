package cl.ventamobile.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cl.ventamobile.constant.VistaConstant;

@Controller
@RequestMapping("/index")
public class IndexController {
	
	@RequestMapping("")
	public ModelAndView index(){
		ModelAndView mv = new ModelAndView(VistaConstant.INDEX);
		return mv;
	}

}