package cl.ventamobile.controller.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cl.ventamobile.model.Producto;
import cl.ventamobile.model.Proveedor;
import cl.ventamobile.service.IProductoService;
import cl.ventamobile.service.IProveedorService;
import cl.ventamobile.util.DatatableResult;
import lombok.extern.apachecommons.CommonsLog;

@RestController
@RequestMapping("/rest/producto")
@CommonsLog
public class MantenedorProductosRestController {
	private static final Log LOG = LogFactory.getLog(MantenedorProductosRestController.class);
	
	@Autowired
	@Qualifier("ProductoService")
	IProductoService productoService;
	
	@Autowired
	@Qualifier("ProveedorService")
	IProveedorService proveedorService;
	
	@PostMapping("/listado_productos")
	public ResponseEntity<DatatableResult> getListadoContrato(HttpServletRequest request,
			HttpSession session){
		
		List<Producto> listadoProductos = new ArrayList<Producto>();
		DatatableResult datatableResult = new DatatableResult();
		ArrayList<Object> data = new ArrayList<Object>();

		listadoProductos = productoService.allProducto();
		for (Producto productoTable : listadoProductos) {
			ArrayList<Object> fila = new ArrayList<Object>();
			fila.add(productoTable.getId());
			fila.add(productoTable.getNombreProducto());
			fila.add(productoTable.getDescripcion());
			fila.add(productoTable.getMontoFormated(productoTable.getPrecioproducto()));
			String editar = "<button id=\"editarProducto\" data-id='" + productoTable.getId() +"' type=\"button\" class=\"radius bordered button btn-edita\"><span data-tooltip class=\"top\" tabindex=\"2\" title=\"Editar\"><i class=\"fas fa-edit\"></i></i></span></button>";
			String eliminar = "<button id=\"eliminarProducto\" data-id='" + productoTable.getId() +"' type=\"button\" class=\"radius bordered alert button btn-danger\"><span data-tooltip class=\"top\" tabindex=\"2\" title=\"Eliminar\"><i class=\"fas fa-trash-alt\"></i></span></button>";
			fila.add(editar + eliminar);
			data.add(fila);
		}
		datatableResult.setData(data);
		return new ResponseEntity<DatatableResult> (datatableResult,HttpStatus.OK);
	}
	
	@GetMapping("/proveedores")
	public List<Proveedor> getProveedores(){
		return proveedorService.allProveedor();
	}
	
	@PostMapping("/save")
	public ResponseEntity<HashMap<String, Object>> saveProducto(@RequestParam("codigoProducto") String codigoProducto,
			@RequestParam("descripcionProducto") String descripcionProducto,
			@RequestParam("precioProducto") int precioProducto,
			@RequestParam("selectProveedor") int selectProveedor,
			@RequestParam("stock") int stock){
		
		Producto produc = new Producto();
		Producto producResult = new Producto();
		HashMap<String, Object> salida = new HashMap<>();
		
		produc.setCantidad(stock);
		produc.setCodigoProducto(codigoProducto);
		produc.setDescripcion(descripcionProducto);
		produc.setIdProveedor(selectProveedor);
		produc.setPrecioproducto(precioProducto);
		produc.setNombreProducto(descripcionProducto);
		
		producResult = productoService.save(produc);
		log.info(producResult);
		if(producResult.getDescripcion() != "") {
			salida.put("resultado", true);
		} else {
			salida.put("resultado", false);
		}
		
		return new ResponseEntity<HashMap<String, Object>>(salida, HttpStatus.OK);
	}
	
	@PostMapping("/delete")
	public boolean deleteProducto(@RequestParam("id") int id){
		return productoService.deleteProducto(id);
	}
	
	@PostMapping("/get")
	public ResponseEntity<HashMap<String, Object>> getExclusionServicio(@RequestParam("id") int id){
		HashMap<String, Object> salida = new HashMap<>();
		Producto producto = new Producto();
		producto = productoService.getProducto(id);
		
		if (producto.getDescripcion() != null) {
			salida.put("pro", producto);
			salida.put("resultado", true);
		} else {
			salida.put("pro", producto);
			salida.put("resultado", false);
		}
		return new ResponseEntity<HashMap<String, Object>>(salida, HttpStatus.OK);
	}
	
	@PostMapping("/update")
	public ResponseEntity<HashMap<String, Object>> saveProducto(@RequestParam("codigoProducto") String codigoProducto,
			@RequestParam("descripcionProducto") String descripcionProducto,
			@RequestParam("precioProducto") int precioProducto,
			@RequestParam("selectProveedor") int selectProveedor,
			@RequestParam("stock") int stock,
			@RequestParam("id") int id){
		
		Producto produc = new Producto();
		Producto producResult = new Producto();
		HashMap<String, Object> salida = new HashMap<>();
		
		produc.setId(id);
		produc.setCantidad(stock);
		produc.setCodigoProducto(codigoProducto);
		produc.setDescripcion(descripcionProducto);
		produc.setIdProveedor(selectProveedor);
		produc.setPrecioproducto(precioProducto);
		produc.setNombreProducto(descripcionProducto);
		
		producResult = productoService.update(produc);
		log.info(producResult);
		if(producResult.getDescripcion() != "") {
			salida.put("resultado", true);
		} else {
			salida.put("resultado", false);
		}
		
		return new ResponseEntity<HashMap<String, Object>>(salida, HttpStatus.OK);
	}
}
