package cl.ventamobile.controller.rest;

import java.util.HashMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cl.ventamobile.service.impl.UsuarioService;


@RestController
@RequestMapping("/rest/login")
public class LoginRestController {
	
	private static final Log LOG = LogFactory.getLog(LoginRestController.class);
	
	@Autowired
	UsuarioService usuarioService;
	
	@PostMapping("/validaLogin")
	public ResponseEntity<HashMap<String, Object>> validaLogin(@RequestParam("username") String username, @RequestParam("password") String password){
		HashMap<String, Object> salida = new HashMap<>();
		salida.put("validar", usuarioService.esUsuario(username, password));
		salida.put("usuario", "ividal");
		return new  ResponseEntity<HashMap<String, Object>>(salida, HttpStatus.OK);
	}
}
