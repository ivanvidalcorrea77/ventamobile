package cl.ventamobile.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.ventamobile.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, String> {
	boolean existsByUsuarioAndContrasena(String usuario, String contrasena);
}
