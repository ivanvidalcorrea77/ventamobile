package cl.ventamobile.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.ventamobile.model.Producto;

public interface ProductoRepository extends JpaRepository<Producto, Integer> {

}
