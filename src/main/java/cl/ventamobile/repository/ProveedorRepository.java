package cl.ventamobile.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.ventamobile.model.Proveedor;

public interface ProveedorRepository extends JpaRepository<Proveedor, Integer> {

}