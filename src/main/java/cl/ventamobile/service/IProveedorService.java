package cl.ventamobile.service;

import java.util.List;

import cl.ventamobile.model.Proveedor;

public interface IProveedorService {
	List<Proveedor> allProveedor();
	Proveedor getProveedor(int id);
}
