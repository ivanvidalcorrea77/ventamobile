package cl.ventamobile.service;

import java.util.List;

import cl.ventamobile.model.Producto;

public interface IProductoService {
	List<Producto> allProducto();
	Producto getProducto(int id);
	public Producto save(Producto productObj);
	boolean deleteProducto(int id);
	public Producto update(Producto productObj);
}
