package cl.ventamobile.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.ventamobile.repository.UsuarioRepository;
import cl.ventamobile.service.IUsuarioService;
import lombok.extern.apachecommons.CommonsLog;

@Service("UsuarioService")
@CommonsLog
public class UsuarioService implements IUsuarioService {

	@Autowired
	UsuarioRepository usuarioRepo;
	
	@Override
	public boolean esUsuario(String usuario, String contrasena) {
		try {
			return usuarioRepo.existsByUsuarioAndContrasena(usuario, contrasena);
		} catch (Exception e) {
			log.error(e.getMessage());
			return false;
		}
	}

}
