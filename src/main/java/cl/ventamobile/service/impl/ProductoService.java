package cl.ventamobile.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.ventamobile.model.Producto;
import cl.ventamobile.repository.ProductoRepository;
import cl.ventamobile.service.IProductoService;
import lombok.extern.apachecommons.CommonsLog;

@Service("ProductoService")
@CommonsLog
public class ProductoService implements IProductoService {

	@Autowired
	ProductoRepository productoRepo;
	
	@Override
	public List<Producto> allProducto() {
		List<Producto> productos = new ArrayList<Producto>();
		try {
			return productoRepo.findAll();
		} catch (Exception e) {
			log.error(e.getMessage());
			return productos;
		}
	}

	@Override
	public Producto getProducto(int id) {
		Producto salida = new Producto();
		try {
			return productoRepo.findOne(id);
		} catch (Exception e) {
			log.error(e.getMessage());
			return salida;
		}
	}

	@Override
	public boolean deleteProducto(int id) {
		try {
			productoRepo.delete(id);
			return true;
		} catch (Exception e) {
			log.error(e.getMessage());
			return false;
		}
	}

	@Override
	public Producto save(Producto entity) {
		try {
			return productoRepo.save(entity);
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
	}

	@Override
	public Producto update(Producto productObj) {
		try {
			Producto productoToUpdate = productoRepo.getOne(productObj.getId());
			productoToUpdate.setCantidad(productObj.getCantidad());
			productoToUpdate.setCodigoProducto(productObj.getCodigoProducto());
			productoToUpdate.setDescripcion(productObj.getDescripcion());
			productoToUpdate.setIdProveedor(productObj.getIdProveedor());
			productoToUpdate.setNombreProducto(productObj.getNombreProducto());
			productoToUpdate.setPrecioproducto(productObj.getPrecioproducto());
			return productoRepo.save(productoToUpdate);
		} catch (Exception e) {
			log.error(e.getMessage());
			return null;
		}
	}

}
