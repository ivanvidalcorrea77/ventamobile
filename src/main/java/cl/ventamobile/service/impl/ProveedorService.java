package cl.ventamobile.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.ventamobile.model.Producto;
import cl.ventamobile.model.Proveedor;
import cl.ventamobile.repository.ProveedorRepository;
import cl.ventamobile.service.IProveedorService;
import lombok.extern.apachecommons.CommonsLog;

@Service("ProveedorService")
@CommonsLog
public class ProveedorService implements IProveedorService {

	@Autowired
	ProveedorRepository proveedorRepo;
	
	@Override
	public List<Proveedor> allProveedor() {
		List<Proveedor> proveedores = new ArrayList<Proveedor>();
		try {
			return proveedorRepo.findAll();
		} catch (Exception e) {
			log.error(e.getMessage());
			return proveedores;
		}
	}

	@Override
	public Proveedor getProveedor(int id) {
		Proveedor salida = new Proveedor();
		try {
			return proveedorRepo.findOne(id);
		} catch (Exception e) {
			log.error(e.getMessage());
			return salida;
		}
	}

}
