$(document).ready(function(){
	_ejecutarValidarUsuario = function(){
		$.post("/rest/login/validaLogin", {
			username: $("#username").val(),
			password: $("#password").val()
		})
		.done(function(data){
			if(data.validar){
				$(location).attr('href', 'http://dev.uft.cl:8080/index')
			} else {
				swal("Error", "Usuario o Contraseña no validas", "error");
			}
		})
		.fail(function() {
			swal("Error", "Usuario o Contraseña no validas", "error");
		});
	}
	
	$("#btnIngresar").click(function(e){
		e.preventDefault();
		_ejecutarValidarUsuario();
	});
});