$(document).ready(function(){
	var cargarGrillaProductos = function(){
		grillaProductos = $("#datatable-productos").DataTable({
			"processing": true,
			"serverSide": true,
			"bLengthChange": false,
			"paging": false,
			"bInfo": false,
			"ajax": {
				"url": "/rest/producto/listado_productos",
				"type": "POST"
			},
			"aoColumns": [{"bSortable": false},
				{"bSortable": false},
				{"bSortable": false},
				{"bSortable": false},
				{"bSortable": false},
			],
			"order": [[1, 'desc']],
	        "bFilter": false,
	        "drawCallback" : function (settings) {
	        	$(".btn-edita").off("click").on("click", function(){
	        		var id = $(this).data("id");
	        		_ejecutarExtraerProducto(id);
	        	});
	        	$(".btn-danger").off("click").on("click", function(){
	        		var id = $(this).data("id");
	        		swal({
	        			  title: "¿Esta Seguro que desea eliminar el producto?",
	        			  icon: "warning",
	        			  buttons: true,
	        			  dangerMode: true,
	        			})
	        			.then((willDelete) => {
	        			  if (willDelete) {
	        				_ejecutarEliminarProducto(id);
	        			    swal("El producto ha sido eliminado correctamente", {
	        			      icon: "success",
	        			    })
	        			    .then((value) => {
	        			    	$(location).attr('href', 'http://dev.uft.cl:8080/mantenedor/productos')
	        			    });
	        			  }
	        			});
	        	});
	        }
		});
	}
	
	_ejecutarEliminarProducto = function(id){
		$.post("/rest/producto/delete", {
			id : id
		})
		.done(function(data){
			console.log(data);
		})
		.fail(function(){
			swal("Error", "No se pudo elimiar producto", "error");
		})
	}
	
	_ejecutarExtraerProducto = function(id){
		$.post("/rest/producto/get", {
			id : id
		})
		.done(function(data){
			console.log(data);
			$("#codigoProducto").val(data.pro.codigoProducto);
			$("#descripcionProducto").val(data.pro.descripcion);
			$("#precioProducto").val(data.pro.precioproducto);
			$("#selectProveedor").val(data.pro.idProveedor);
			$("#stock").val(data.pro.cantidad);
			$("#idProducto").val(data.pro.id);
			$("#Modal_Crear").foundation("open");
		})
		.fail(function(){
			swal("Error", "No se pudo extraer producto", "error");
		})
	}
	
	_ejecutarObtenerProveedores = function(){
		$.get( "/rest/producto/proveedores", function( data ) {
			var $sel = $("#selectProveedor");
			$sel.empty();
			$sel.append($("<option selected=\"selected\" value=\"-1\">Seleccionar</option>"));
			$.each(data, function(id,comuna){
				$sel.append($("<option></option>").attr("value",comuna.id).text(comuna.descripcion));
            });
		});		
	}
	
	_ejecutarGuardarProducto = function(){
		$.post("/rest/producto/save", {
			codigoProducto : $("#codigoProducto").val(),
			descripcionProducto : $("#descripcionProducto").val(),
			precioProducto : $("#precioProducto").val(),
			selectProveedor : $("#selectProveedor").val(),
			stock : $("#stock").val(),
		})
		.done(function(data){
			console.log(data);
			if(data.resultado){
				swal("Exito", "Producto guardado correctamente", "success").then((value) => {
					$(location).attr('href', 'http://dev.uft.cl:8080/mantenedor/productos')
				});
			} else {
				swal("Error", "No se pudo guardar producto", "error");
			}
		})
		.fail(function(){
			swal("Error", "No se pudo guardar producto", "error");
		})
	}
	
	_ejecutarUpdateProducto = function(id){
		$.post("/rest/producto/update", {
			codigoProducto : $("#codigoProducto").val(),
			descripcionProducto : $("#descripcionProducto").val(),
			precioProducto : $("#precioProducto").val(),
			selectProveedor : $("#selectProveedor").val(),
			stock : $("#stock").val(),
			id : $("#idProducto").val(),
		})
		.done(function(data){
			console.log(data);
			if(data.resultado){
				$("#idProducto").val(0);
				swal("Exito", "Producto actualizado correctamente", "success").then((value) => {
					$(location).attr('href', 'http://dev.uft.cl:8080/mantenedor/productos')
				});
			} else {
				swal("Error", "No se pudo actualizar producto", "error");
			}
		})
		.fail(function(){
			swal("Error", "No se pudo actualizar producto", "error");
		})
	}
	
	cargarGrillaProductos();
	_ejecutarObtenerProveedores();
	
	$("#saveProducto").click(function(e){
		var idProduc = $("#idProducto").val();
		e.preventDefault();
		if (idProduc == 0) {
			_ejecutarGuardarProducto();
		} else {
			_ejecutarUpdateProducto();
		}
		
	});
});